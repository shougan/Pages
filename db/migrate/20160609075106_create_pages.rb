class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :m_title
      t.text :m_desc
      t.integer :position

      t.timestamps null: false
    end
  end
end

class CreatePagesWidgets < ActiveRecord::Migration
  def change
    create_table :pages_widgets do |t|
      t.integer :page_id
      t.integer :widget_id

      t.timestamps null: false
    end
  end
end

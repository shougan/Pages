class CreateWidgets < ActiveRecord::Migration
  def change
    create_table :widgets do |t|
      t.integer :position
      t.references :element, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end

class CreateWidgetAttrs < ActiveRecord::Migration
  def change
    create_table :widget_attrs do |t|
      t.references :widget, foreign_key: true
      t.string :name
      t.string :value
      t.integer :atype

      t.timestamps null: false
    end
  end
end
